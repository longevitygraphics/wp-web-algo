<?php
/**
 * The header for our theme
 *
 */

?>

<?php do_action('document_start'); ?>

<!doctype html>
<html <?php language_attributes(); ?> <?php do_action('html_class'); ?>>
<head>
  <!--[if lt IE 9]>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
  <![endif]-->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
  <?php do_action('wp_header'); ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

  <?php do_action('wp_body_start'); ?>

  <div id="page" class="site <?php if( (is_product_category() && get_field('footer_to_bottom') == true) || (is_page() && get_field('footer_to_bottom') == true) || (is_product() && get_field('footer_to_bottom') == true) ){echo 'footer-to-bottom'; } ?>">

  	<header id="site-header">

      <?php do_action('wp_utility_bar'); ?>

      <div class="header-main container">
        <div class="d-flex justify-content-between align-items-center flex-wrap">
          <div class="site-branding px-3 py-4 py-lg-2">
            <div class="logo">
              <a href="/"><?php echo site_logo(); ?></a>
            </div>
            <div class="mobile-toggle"><i class="fa fa-bars" aria-hidden="true"></i></div>
          </div><!-- .site-branding -->

          <?php get_template_part("/templates/template-parts/header/main-nav"); ?>
        </div>
      </div>
      <?php
       $subpage_var         = get_query_var( 'subpage' );
       $page_is_add_to_cart = ( isset( $subpage_var ) and ! empty( $subpage_var ) ) ? true : false;
      ?>
      <?php if (is_product() && !$page_is_add_to_cart): ?>
          <div class="product-resource-button">
            <a href="" class="btn btn-primary">Product Resources</a>
          </div>
      <?php endif ?>

    </header><!-- #masthead -->

    <div id="site-content" role="main">
      <?php do_action('wp_content_top'); ?>
