	<div class="accordion faq-groups">

		<?php if($faqs && is_array($faqs) && sizeof($faqs) > 0): ?>
			<?php foreach ($faqs as $key => $faq): ?>
		  	<div class="card">
		    	<div class="card-header" id="headingOne">
			     	<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?php echo $faq_group_index . $key; ?>" aria-expanded="false" aria-controls="collapse<?php echo $faq_group_index . $key; ?>">
				       	<?php echo $faq['title']; ?>
				    </button>	
				    <i class="fas fa-angle-up"></i>
			    </div>

			    <div id="collapse<?php echo $faq_group_index . $key; ?>" class="collapse" aria-labelledby="heading<?php echo $faq_group_index . $key; ?>">
			      	<div class="card-body">
			        	<?php echo $faq['content']; ?>
			      	</div>
			    </div>
		  	</div>
		  	<?php endforeach; ?>
		<?php endif; ?>

	</div>