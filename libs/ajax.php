<?php

use Longevity\Framework\Plugins;
use Longevity\Framework\Helper;
use Longevity\Framework\Fontawesome;
if ( ! function_exists('lg_write_log')) {
     function lg_write_log ( $log )  {
        if ( is_array( $log ) || is_object( $log ) ) {
           error_log( print_r( $log, true ) );
        } else {
           error_log( $log );
        }
     }
  }

	class LGChildAjax{

		function __construct(){
			add_action( 'wp_ajax_lg_get_product_categories', array( $this, 'lg_get_product_categories' ) );
			add_action( 'wp_ajax_nopriv_lg_get_product_categories', array( $this, 'lg_get_product_categories' ) );

			add_action( 'wp_ajax_lg_get_products_by_categories', array( $this, 'lg_get_products_by_categories' ) );
			add_action( 'wp_ajax_nopriv_lg_get_products_by_categories', array( $this, 'lg_get_products_by_categories' ) );

			add_action( 'wp_ajax_lg_get_product_name_by_slug', array( $this, 'lg_get_product_name_by_slug' ) );
			add_action( 'wp_ajax_nopriv_lg_get_product_name_by_slug', array( $this, 'lg_get_product_name_by_slug' ) );
		}

		function lg_get_products_by_categories(){
			$data = $_REQUEST['data']['product_cat'];
			$args = array(
				'post_type' => 'product',
				'tax_query' => array(
					array(
						'taxonomy' => 'product_cat',
						'field'    => 'slug',
						'terms'    => $data,
					),
				),
			);
			$query = new WP_Query( $args );
			$posts = $query->posts;

			wp_send_json($posts);
		}

		function lg_get_product_categories(){
			$terms = get_terms( 'product_cat');
			
			wp_send_json($terms);
		}

		function lg_get_product_name_by_slug(){
			$data = $_REQUEST['data']['product_slug'];
			$product = $this->get_post_by_slug($data);

			wp_send_json($product->post_title);
		}

		function get_post_by_slug($slug){
			lg_write_log($slug);
		    $posts = get_posts(array(
		            'name' => $slug,
		            'posts_per_page' => 1,
		            'post_type' => 'product',
		            'post_status' => 'publish'
		    ));
		    
		    if(! $posts ) {
		        throw new Exception("NoSuchPostBySpecifiedID");
		    }

		    return $posts[0];
		}

	}

	new LGChildAjax();

?>