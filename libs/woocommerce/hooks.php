<?php

add_action( 'woocommerce_single_product_summary', 'lg_product_short_description', 30 );
add_action( 'lg_woocommerce_product_sidebar', 'lg_woocommerce_add_to_cart', 10 );
add_action( 'lg_woocommerce_product_sidebar', 'lg_woocommerce_add_to_cart_after', 30 );
add_action( 'woocommerce_before_add_to_cart_quantity', 'lg_woocommerce_product_quantity_before' );
add_action( 'woocommerce_after_add_to_cart_quantity', 'lg_woocommerce_product_quantity_after' );

add_theme_support( 'woocommerce' );
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );


//add_action( 'lg_woocommerce_product_sidebar', 'woocommerce_template_single_add_to_cart', 20 );

//remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

function disable_woo_commerce_sidebar() {
	remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10); 
}

remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);

add_action('init', 'disable_woo_commerce_sidebar');


add_filter('woocommerce_is_purchasable', 'connot_purchase', 10, 2);

function connot_purchase( $is_purchasable, $object ) {
    // this is a field added using 'Advance Custom Fields' plugin 
    $non_purchasable = get_field('non_purchasable', $object->get_id()); 

    if($non_purchasable || !$is_purchasable)
        return false;
    else
        return true;
}

function lg_woocommerce_product_quantity_before() {
	ob_start(); ?>
	<div><span>QUANTITY: </span>
	<?php echo ob_get_clean();
}

function lg_woocommerce_product_quantity_after() {
	ob_start(); ?>
	</div>
	<?php echo ob_get_clean();
}


function lg_product_short_description() {
	ob_start(); ?>
	<div class=" pb-4 product-short-description">
		<?php echo get_field( 'short_description' ); ?>
	</div>
	<?php echo ob_get_clean();
}

function lg_woocommerce_add_to_cart() {
	global $post;
	$product = wc_get_product( $post->ID );
	$title   = $product->get_title();
	$price   = $product->get_price_html();

	echo "<div class='lg-add-to-cart-wrap'><div class='lg-add-to-cart'><div class='price'>Product MSRP: $price</div></div>";
}

function lg_woocommerce_add_to_cart_after() {
	echo "</div>";
}

//add the "buy" rewrite rule
add_action( 'init', 'lg_create_buy_rewrite_rule' );
function lg_create_buy_rewrite_rule() {
	add_rewrite_rule( '^product/([a-zA-Z0-9_.-]+)/(buy)/?', 'index.php?product=$matches[1]&subpage=$matches[2]', 'top' );
}

add_filter( 'query_vars', 'lg_query_vars' );
function lg_query_vars( $query_vars ) {
	$query_vars[] = 'subpage';

	return $query_vars;
}

/**
 * Following filter will allow us to edit the add to cart message, we can add extra buttons here
 */
function filter_wc_add_to_cart_message_html( $message, $products ) {
	/*pr($message);
	die;*/
	return $message;
}

;
// add the filter
add_filter( 'wc_add_to_cart_message_html', 'filter_wc_add_to_cart_message_html', 10, 2 );


/**
 * remove the 'woocommerce_cross_sell_display' hook and re-add it to 'woocommerce_after_cart'
 */
remove_action( "woocommerce_cart_collaterals", "woocommerce_cross_sell_display" );
add_filter( "woocommerce_cross_sells_columns", function () {
	return 6;
} );
add_action( "woocommerce_after_cart", "woocommerce_cross_sell_display", 10 );


/**
 * Add the Product Short Description to Product Archive Page
 */
add_action( 'woocommerce_after_shop_loop_item_title', 'lg_excerpt_in_product_archives', 40 );
function lg_excerpt_in_product_archives() {
	the_excerpt();
}

/**
 * remove woocommerce tabs
 */
remove_action( 'woocommerce_after_single_product_summary','woocommerce_output_product_data_tabs' );


// Change "You may also like..." text in WooCommerce
add_filter('gettext', 'lg_change_up_sell_title');
function lg_change_up_sell_title($translated)
{
	$translated = str_ireplace('You may also like&hellip;', 'Related Products', $translated);
	return $translated;
}


// Change how cross sell products are ordered
add_filter( 'woocommerce_cross_sells_orderby', 'filter_cross_sells_orderby', 10, 1 );
function filter_cross_sells_orderby( $orderby ){
    return 'menu_order'; // Default is 'rand'
}

// remove the short description on Single Product page
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );

function woocommerce_template_single_excerpt() {
        return;
}

// change the add to cart text

add_filter( 'woocommerce_product_add_to_cart_text' , 'custom_woocommerce_product_add_to_cart_text' );
/**
 * custom_woocommerce_template_loop_add_to_cart
*/
function custom_woocommerce_product_add_to_cart_text() {
	global $product;
	
	$product_type = $product->product_type;
	
	switch ( $product_type ) {
		case 'external':
			return __( 'Buy Product', 'woocommerce' );
		break;
		case 'grouped':
			return __( 'View Products', 'woocommerce' );
		break;
		case 'simple':
			return __( 'Add to Cart', 'woocommerce' );
		break;
		case 'variable':
			return __( 'Select Options', 'woocommerce' );
		break;
		default:
			return __( 'Read More', 'woocommerce' );
	}
	
}

// change 'continue shopping' link
add_filter( 'woocommerce_continue_shopping_redirect', 'lg_change_continue_shopping' );
 
function lg_change_continue_shopping() {
	return '/algo-store';
}