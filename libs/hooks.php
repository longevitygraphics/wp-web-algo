<?php
  if ( ! function_exists('lg_write_log')) {
     function lg_write_log ( $log )  {
        if ( is_array( $log ) || is_object( $log ) ) {
           error_log( print_r( $log, true ) );
        } else {
           error_log( $log );
        }
     }
  }

  function faq_layout( $title, $field, $layout, $i ) {

    if($layout['name'] == 'group_title'){
      return get_sub_field('group_title');
    }elseif($layout['name'] == 'group_faq'){
      return 'FAQ Group';
    }else{
      return $title;
    }
    
  }

  function featured_banner_top(){
      ob_start(); ?>
        <?php get_template_part( '/templates/template-parts/page/feature-slider' ); ?> 
      <?php echo ob_get_clean();
  }

  // name
  add_filter('acf/fields/flexible_content/layout_title', 'faq_layout', 20, 4);
  add_action('wp_content_top', 'featured_banner_top');

?>