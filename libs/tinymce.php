<?php

	global $lg_tinymce_custom;
	
	$lg_tinymce_custom = array(
	    'title' => 'Custom Style',
	    'items' =>  array(
	    	array(
				'title' => 'Dark Bullet',
	            'selector' => 'ul, ol',
	            'classes' => 'dark-bullet'
			),
			array(
				'title' => 'White Bullet',
	            'selector' => 'ul, ol',
	            'classes' => 'white-bullet'
			),
			array(
				'title' => 'Square',
	            'selector' => 'ul, ol',
	            'classes' => 'square-bullet'
			),
			array(
				'title' => 'Underline',
	        	'selector' => 'h1, h2, h3, h4, h5, h6, p, span, li, a',
	        	'classes' => 'underline-hover'
			)
	    )
	    
	);

?>