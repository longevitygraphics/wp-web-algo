<?php

//Custom Post Types
function create_post_type() {

    // SERVICES
    register_post_type( 'news_events',
        array(
          'labels' => array(
            'name' => __( 'News & Events' ),
            'singular_name' => __( 'News & Events' )
          ),
          'public' => true,
          'has_archive' => false,
          "rewrite" => array("with_front" => false, "slug" => 'news_events'),
          'show_in_menu'    => 'lg_menu',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );

}
add_action( 'init', 'create_post_type' );

?>