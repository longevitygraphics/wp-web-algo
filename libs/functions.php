<?php

function main_nav_items( $items, $args ) {
	if ( $args->theme_location == 'top-nav' ) {
		$items .= '<li class="d-none d-lg-inline-block menu-item menu-item-type-custom menu-item-object-custom nav-item ml-3"><a class="nav-link text-white btn btn-primary" href="/cart"><i class="fas fa-shopping-cart"></i> Cart</a></li>';
		$items .= '<li class="d-lg-none nmenu-item menu-item-type-custom menu-item-object-custom nav-item"><a class="nav-link text-white " href="/cart"><i class="fas fa-shopping-cart"></i> Cart</a></li>';
	}

	return $items;
}

add_filter( 'wp_nav_menu_items', 'main_nav_items', 10, 2 );

if ( isset( $_GET['media'] ) ) {
	add_filter( 'gform_confirmation_2', 'custom_confirmation_message', 10, 4 );
}

function custom_confirmation_message( $confirmation, $form, $entry, $ajax ) {
	if ( isset( $_GET['media'] ) ) {
		$media_id = $_GET['media'];
	}
	$media_url    = wp_get_attachment_url( $media_id );
	$media_title  = get_the_title( $media_id );
	$confirmation = '<h2>Thank you for filling out the form, click <a target="_blank" href="' . $media_url . '">here</a> to download the file</h2>';

	return $confirmation;
}

function excerpt( $num, $link ) {
	$limit   = $num + 1;
	$excerpt = explode( ' ', get_the_excerpt(), $limit );
	array_pop( $excerpt );
	$excerpt = implode( ' ', $excerpt );
	echo $excerpt;
}

function pr( $d ) {
	echo '<pre>';
	print_r( $d );
	echo '</pre>';
}

function lg_numeric_posts_nav() {

	if ( is_singular() ) {
		return;
	}

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if ( $wp_query->max_num_pages <= 1 ) {
		return;
	}

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/** Add current page to the array */
	if ( $paged >= 1 ) {
		$links[] = $paged;
	}

	/** Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<div class="pagination"><ul>' . "\n";

	/** Previous Post Link */
	if ( get_previous_posts_link() ) {
		printf( '<li>%s</li>' . "\n", get_previous_posts_link() );
	}

	/** Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';

		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) ) {
			echo '<li>…</li>';
		}
	}

	/** Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/** Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) ) {
			echo '<li>…</li>' . "\n";
		}

		$class = $paged == $max ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/** Next Post Link */
	if ( get_next_posts_link() ) {
		printf( '<li>%s</li>' . "\n", get_next_posts_link() );
	}

	echo '</ul></div>' . "\n";

}
