// Windows Load Handler

(function($) {

    $(window).on('load', function(){

        $('.min-screen-height').css('min-height', 'calc(100vh - ' + ($('#site-header').outerHeight() + $('#site-footer').outerHeight()) + 'px)');
        
			$('.nav-tabs').find('.nav-item:first-child .nav-link').addClass('active');
			$('.tab-content').find('.tab-pane:first-child').addClass('show active');

			/*$('.buy-online').on('click', function (e) {
				e.preventDefault();

				$('.lg-add-to-cart-wrap').toggleClass('show');
			});*/

			$('.product-sidebar .fa-times').on('click', function () {
				$('.product-sidebar').hide();
			});

			$('.product-resource-button a').on('click', function (e) {
				e.preventDefault();

				$('.product-sidebar').show();
			});

			// fix cart page change address toggle issue
			if($('.shipping-calculator-button')){
				console.log('shipping calculator detected')
				$('.shipping-calculator-button').attr('href', '');
			}

    });

}(jQuery));