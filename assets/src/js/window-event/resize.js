// Window Resize Handler

(function($) {

    $(window).on('resize', function(){

    	var container = $('.woocommerce.single-product .container');
    	var gap = ($(window).width() - container.outerWidth()) / 2 - 15;
        var header_height = $('#site-header').outerHeight();
        var window_top = $(window).scrollTop();

    	if(gap < 300 && $(window).width() > 991 && !$('.single-product-wrap').hasClass('add-to-cart-page')){
    		$('.woocommerce.single-product .container').css('padding-right', (300 - gap) + 'px');
    	}else{
    		$('.woocommerce.single-product .container').css('padding-right', 15);
    	}
    })

}(jQuery));