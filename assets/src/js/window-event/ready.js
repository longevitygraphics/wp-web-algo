// Windows Ready Handler
 	
(function($) {
	
    $(document).ready(function(){

    	var container = $('.woocommerce.single-product .container');
    	var gap = ($(window).width() - container.outerWidth()) / 2 - 15;
        var header_height = $('#site-header').outerHeight();
        var window_top = $(window).scrollTop();

    	if(gap < 300 && $(window).width() > 991 && !$('.single-product-wrap').hasClass('add-to-cart-page')){
    		$('.woocommerce.single-product .container').css('padding-right', (300 - gap) + 'px');
    	}

        // if( window_top < header_height){
        //     $('.single-product .single-product-wrap .product-sidebar > div').css('margin-top', (header_height - window_top));
        // }else{
        //     $('.single-product .single-product-wrap .product-sidebar > div').css('margin-top', 0);
        // }

    	if($('.top-banner .gallery')[0]){
    		$('.top-banner .gallery').slick({
			  arrows: true,
			  autoplay: true,
  			  autoplaySpeed: 5000
			});
    	}
        // add class to thank you page #page
        $('.thank-you').parents('#page').addClass('thank-you-page');

        // footer to bottom for insufficient content page
        // if($('#page').hasClass('footer-to-bottom') || $('#page').hasClass('thank-you-page')){
        //     var site_footer = $('#site-footer');
        //     var site_content = $('#site-content');
        //     var footerHeight = site_footer.outerHeight();
        //     site_content.css('paddingBottom', footerHeight);
        // }

        // toggle single product tabs on click
        $('.single-product-details .nav-item > .nav-link').click(function(){
            let selected = $(this).text();
           $(this).parents('.nav-tabs').prev('.mobile-tab-btn').text(selected)
        })
        $('.mobile-tab-btn').click(function(){
            $(this).next('.nav-tabs').slideToggle();
            $(this).toggleClass('open-tabs');
        })
    });
}(jQuery));