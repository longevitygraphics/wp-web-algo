function footer_to_bottom_initialize( callback ){
	var site_footer = $('#site-footer');
	var site_content = $('#site-content');
	var footerHeight = site_footer.outerHeight();

	site_content.css('paddingBottom', footerHeight);

	if(callback != null){
		callback();
	}
	
}