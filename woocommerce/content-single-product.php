<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.

	return;
}

//check if the page is "add to cart"
$subpage_var         = get_query_var( 'subpage' );
$page_is_add_to_cart = ( isset( $subpage_var ) and ! empty( $subpage_var ) ) ? true : false;


//remove related products from single product summary.
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
if ( $page_is_add_to_cart ) {
	//add cross sells on product cart page
	/*add_filter( "woocommerce_cross_sells_columns", function () {
		return 4;
	} );
	add_action( "woocommerce_after_single_product", "woocommerce_cross_sell_display", 10 );*/

	//add related products on product cart page
	//add_action( "woocommerce_after_single_product", "woocommerce_output_related_products", 20 );




} else {
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
	remove_action('woocommerce_after_single_product_summary','woocommerce_upsell_display', 15);
}
?>

<div class="single-product-wrap <?php if($page_is_add_to_cart){ echo 'add-to-cart-page'; }?>">

	<div class="container">
		<div id="product-<?php the_ID(); ?>" <?php wc_product_class(); ?>>

			<?php
			/**
			 * Hook: woocommerce_before_single_product_summary.
			 *
			 * @hooked woocommerce_show_product_sale_flash - 10
			 * @hooked woocommerce_show_product_images - 20
			 */
			do_action( 'woocommerce_before_single_product_summary' );
			?>

			<div class="summary entry-summary">
				<?php
				/**
				 * Hook: woocommerce_single_product_summary.
				 *
				 * @hooked woocommerce_template_single_title - 5
				 * @hooked woocommerce_template_single_rating - 10
				 * @hooked woocommerce_template_single_price - 10
				 * @hooked woocommerce_template_single_excerpt - 20
				 * @hooked woocommerce_template_single_add_to_cart - 30
				 * @hooked woocommerce_template_single_meta - 40
				 * @hooked woocommerce_template_single_sharing - 50
				 * @hooked WC_Structured_Data::generate_product_data() - 60
				 */
				do_action( 'woocommerce_single_product_summary' );
				?>
			</div>

			<?php
			/**
			 * Hook: woocommerce_after_single_product_summary.
			 *
			 * @hooked woocommerce_output_product_data_tabs - 10
			 * @hooked woocommerce_upsell_display - 15
			 * @hooked woocommerce_output_related_products - 20
			 */
			do_action( 'woocommerce_after_single_product_summary' );
			?>
			<!-- show buy online button if it isn't add to cart page on mobile -->
			<?php if(!$page_is_add_to_cart) : ?>
			<?php
			$terms = get_the_terms( $post, 'product_cat' );

			$current_page_url     = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			$add_to_cart_page_url = strtok( $current_page_url, "?" );
			$add_to_cart_page_url .= '/buy/';
			?>
			
			<div class="d-lg-none">
				<a href="//<?php echo $add_to_cart_page_url; ?>" class="btn btn-primary buy-online">BUY ONLINE</a>
			</div>
			<?php endif; ?>
			<!-- end of show buy online button if it isn't add to cart page on mobile -->
		</div>

		<?php
		if ( ! $page_is_add_to_cart ) {
			$current_post = $post;
			include( locate_template( '/templates/template-parts/woocommerce/single-product-sidebar.php' ) );
		}
		?>
	</div>
</div>

<?php
$long_description_active     = get_field( 'long_description_active' ) == 1 ? true : false;
$feature_active              = get_field( 'feature_active' ) == 1 ? true : false;
$specifications_active       = get_field( 'specifications_active' ) == 1 ? true : false;
$applications_active         = get_field( 'applications_active' ) == 1 ? true : false;
$compatibility_active        = get_field( 'compatibility_active' ) == 1 ? true : false;
$ordering_information_active = get_field( 'ordering_information_active' ) == 1 ? true : false;

$long_description     = get_field( 'long_description' );
$features             = get_field( 'features' );
$specifications       = get_field( 'specifications' );
$applications         = get_field( 'applications' );
$compatibility        = get_field( 'compatibility' );
$ordering_information = get_field( 'ordering_information' );
?>

<?php if ( ! $page_is_add_to_cart ) : ?>
	<div class="single-product-details">
		<div class="tabs bg-gray-dark">
			<div class="container">
				<?php
					if($long_description_active || $feature_active || $specifications_active || $applications_active || $compatibility_active || $ordering_information_active){
						echo '<button class="btn btn-primary btn-block mobile-tab-btn d-lg-none">Product Details</button>';
					}
				?>
				<ul class="nav nav-tabs" id="myTab" role="tablist">

					<?php if ( $long_description_active ): ?>
						<li class="nav-item">
							<a class="nav-link" id="description-tab" data-toggle="tab" href="#description" role="tab"
							   aria-controls="description" aria-selected="true">Description</a>
						</li>
					<?php endif; ?>

					<?php if ( $feature_active ): ?>
						<li class="nav-item">
							<a class="nav-link" id="features-tab" data-toggle="tab" href="#features" role="tab"
							   aria-controls="features" aria-selected="false">Features</a>
						</li>
					<?php endif; ?>

					<?php if ( $specifications_active ): ?>
						<li class="nav-item">
							<a class="nav-link" id="specifications-tab" data-toggle="tab" href="#specifications" role="tab"
							   aria-controls="specifications" aria-selected="false">Specifications</a>
						</li>
					<?php endif; ?>

					<?php if ( $applications_active ): ?>
						<li class="nav-item">
							<a class="nav-link" id="applications-tab" data-toggle="tab" href="#applications" role="tab"
							   aria-controls="applications" aria-selected="false">Applications</a>
						</li>
					<?php endif; ?>

					<?php if ( $compatibility_active ): ?>
						<li class="nav-item">
							<a class="nav-link" id="compatibility-tab" data-toggle="tab" href="#compatibility"
							   role="tab"
							   aria-controls="compatibility" aria-selected="false">Compatibility</a>
						</li>
					<?php endif; ?>

					<?php if ( $ordering_information_active ): ?>
						<li class="nav-item">
							<a class="nav-link" id="ordering-information-tab" data-toggle="tab"
							   href="#ordering-information"
							   role="tab" aria-controls="ordering-information" aria-selected="false">Ordering
								Information</a>
						</li>
					<?php endif; ?>

				</ul>
			</div>
		</div>
		<div class="content">
			<div class="container">
				<div class="tab-content" id="myTabContent">

					<?php if ( $long_description_active ): ?>
						<div class="tab-pane fade" id="description" role="tabpanel" aria-labelledby="description-tab">
							<?php
							$description_field = $long_description;
							include( locate_template( '/templates/template-parts/woocommerce/single-product-columns.php' ) );
							?>
						</div>
					<?php endif; ?>

					<?php if ( $feature_active ): ?>
						<div class="tab-pane fade" id="features" role="tabpanel" aria-labelledby="features-tab">
							<?php
							$description_field = $features;
							include( locate_template( '/templates/template-parts/woocommerce/single-product-columns.php' ) );
							?>
						</div>
					<?php endif; ?>

					<?php if ( $specifications_active ): ?>
						<div class="tab-pane fade" id="specifications" role="tabpanel" aria-labelledby="specifications-tab">
							<?php
							$description_field = $specifications;
							include( locate_template( '/templates/template-parts/woocommerce/single-product-columns.php' ) );
							?>
						</div>
					<?php endif; ?>

					<?php if ( $applications_active ): ?>
						<div class="tab-pane fade" id="applications" role="tabpanel" aria-labelledby="applications-tab">
							<?php
							$description_field = $applications;
							include( locate_template( '/templates/template-parts/woocommerce/single-product-columns.php' ) );
							?>
						</div>
					<?php endif; ?>

					<?php if ( $compatibility_active ): ?>
						<div class="tab-pane fade" id="compatibility" role="tabpanel"
						     aria-labelledby="compatibility-tab">
							<?php
							$description_field = $compatibility;
							include( locate_template( '/templates/template-parts/woocommerce/single-product-columns.php' ) );
							?>
						</div>
					<?php endif; ?>

					<?php if ( $ordering_information_active ): ?>
						<div class="tab-pane fade" id="ordering-information" role="tabpanel"
						     aria-labelledby="ordering-information-tab">
							<?php
							$description_field = $ordering_information;
							include( locate_template( '/templates/template-parts/woocommerce/single-product-columns.php' ) );
							?>
						</div>
					<?php endif; ?>

				</div>
			</div>
		</div>
	</div>
<?php endif; ?>

<!-- <div class="product-resource-button">
	<a href="" class="btn btn-primary">Product Resources</a>
</div> -->

<div class="container">
	<?php do_action( 'woocommerce_after_single_product' ); ?>
</div>
