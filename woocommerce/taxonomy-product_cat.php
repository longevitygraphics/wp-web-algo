<?php get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main class="container">
            <h1 class='pt-5'><?php  single_cat_title(); ?></h1>
                <?php if(have_posts()) : ?>
                <div class='d-flex flex-wrap category-products'>
                    <?php while(have_posts()) : the_post(); ?>
                    <div class='col-md-6 col-lg-4 category-product'>
                        <?php
                            $title = get_the_title();
                            $link = get_the_permalink();
                            $id = get_the_ID();
                            $image_url = get_the_post_thumbnail_url();
                            $image_id = get_post_thumbnail_id();
                            $image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
                            $product = wc_get_product( $id );
                        ?>
                        <div class='product-image mb-3'>
                            <?php
                                echo wp_get_attachment_image( $image_id, 'medium' );
                            ?>
                        </div>
                        <div>
                            <h3 class='product-title mb-3'><?php the_title(); ?></h3>
                            <div class="product-excerpt mb-3">
                                <?php excerpt('10', $link); ?>
                            </div>
                            <?php if($product->get_price() && $product->is_purchasable()) : ?>
                                <div class='product-price mb-3'>
                                    <?php echo $product->get_price_html() ?>
                                </div>
                            <?php endif;?>
                            
                           <div class='product-buttons mb-3'>
                               <?php 
                                    if($product->get_price() && $product->is_purchasable()){
                                        if($product->is_type('variable')){
                                            echo '<a class="button product_type_simple add_to_cart_button" href=" ' . $link . '/buy ">';
                                            echo 'Add to Cart';
                                            echo '</a>';
                                        } else {
                                            woocommerce_template_loop_add_to_cart();
                                        }
                                    }
                               ?>
                                <a class="read-more btn btn-primary" href="<?php echo $link; ?>">Read More</a>
                           </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
                <?php endif; ?>
                
                <?php lg_numeric_posts_nav(); ?>
			</main>
		</div>
	</div>

<?php get_footer(); ?>