<?php get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main class="container">
			<h1 class='pt-5'>Algo Store</h1>
			<?php
				$taxonomy     = 'product_cat';
				$orderby      = 'name';  
				$show_count   = 0;      // 1 for yes, 0 for no
				$pad_counts   = 0;      // 1 for yes, 0 for no
				$hierarchical = 1;      // 1 for yes, 0 for no  
				$title        = '';  
				$empty        = 0;

				$args = array(
					'taxonomy'     => $taxonomy,
					'orderby'      => $orderby,
					'show_count'   => $show_count,
					'pad_counts'   => $pad_counts,
					'hierarchical' => $hierarchical,
					'title_li'     => $title,
					'hide_empty'   => $empty
				);
				$all_categories = get_categories( $args );
				?>
				<div class='product-categories d-flex flex-wrap'>
					<?php foreach ($all_categories as $cat) : ?>
						<div class='product-category col-12 col-md-6 col-lg-4'>
							<?php
								$category_id = $cat->term_id;
								$thumbnail_id = get_term_meta( $category_id, 'thumbnail_id', true );
								$image = wp_get_attachment_url( $thumbnail_id );       
							?>
							<?php if($image) : ?>
								<a href="<?php echo get_term_link($cat->slug, 'product_cat'); ?>" class='product-category-image mb-3'>
									<?php
										echo wp_get_attachment_image( $thumbnail_id, 'medium' );
									?>
								</a>
							<?php endif; //end of if image?>
							<h3><a href="<?php echo get_term_link($cat->slug, 'product_cat'); ?>"><?php echo $cat->name; ?></a></h3>
							<div class='product-description'><?php echo $cat->description; ?></div>
						</div>
					<?php endforeach;  //end of foreach($all_categories as $cat)?>
				</div>
			</main>
		</div>
	</div>

<?php get_footer(); ?>