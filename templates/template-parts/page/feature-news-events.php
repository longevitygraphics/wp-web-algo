
<?php
	$args = array(
        'posts_per_page'	=> 4,
        'post_type'		=> 'news_events',
        'nopaging'		=> true
    );

    $result = new WP_Query( $args );

    // Loop
    if ( $result->have_posts() ) :
    	?>
		<div class="news-events-feature row">
    	<?php
        while( $result->have_posts() ) : $result->the_post(); 
    	$title = get_the_title();
    	$content = get_the_content();
    	$link =  get_field('link');
    	$image = get_the_post_thumbnail_url();
    ?>
    	
        <div class="col-lg-3 col-md-6 col-sm-12">
        	<?php if($image): ?>
	        	<div class="image">
	        		<?php if($link): ?><a target="_blank" href="<?php echo $link; ?>"><?php endif; ?>
	        		<img src="<?php echo $image; ?>">
	        		<?php if($link): ?></a><?php endif; ?>
	        	</div>
	        <?php endif; ?>

	        <?php if($content): ?>
				<div class="content text-center">
					<h3><?php echo $title; ?></h3>
					<div><?php echo $content; ?></div>
				</div>
	        <?php endif; ?>
        </div>

		<?php
        endwhile;
        ?>
        </div>
        <div class="d-flex justify-content-center mt-5"><a class="btn btn-outline-primary" href="#">View News & Events</a></div>
    <?php endif; // End Loop

    wp_reset_query();
?>