<?php
	$top_banner_active = get_field('top_banner_active');
	$images = get_field('top_banner');
	$text_overlay = get_field('text_overlay');
	$size = 'full';
?>

<?php if( $images && $top_banner_active == 1 ): ?>
	<div class="top-banner">
		
	    <div class="gallery container">
	        <?php foreach( $images as $image ): ?>
	            <div>
	            	<?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
	            </div>
	        <?php endforeach; ?>
	    </div>
		

		<div class="overlay">
			<div>
				<div>
					<?php echo $text_overlay; ?>
				</div>
			</div>
		</div>
		
	</div>
<?php endif; ?>

<?php if(is_tax("product_cat")) : ?>
	<?php
		$term = get_queried_object();
	?>
	<div class="top-banner">
		<?php 
			$top_banner_images = get_field("top_banner", $term);
			$gallery = 	$top_banner_images;
			$text_overlay = get_field("text_overlay", $term);
		?>
		<?php if($top_banner_images) : ?>
			<div class="top-banner">
			    <div class="gallery">
			        <?php foreach( $top_banner_images as $image ): ?>
			            <div>
			            	<?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
			            </div>
			        <?php endforeach; ?>
			    </div>	
			</div>
		<?php endif; ?>
		<?php if($text_overlay) : ?>
			<div class="overlay">
				<div>
					<div>
						<?php echo $text_overlay; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
<?php endif; //end of if is tax product category?>