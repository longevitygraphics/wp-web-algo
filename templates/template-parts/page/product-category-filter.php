<?php if (isset($_GET['product_name']) && $post = get_page_by_path( $_GET['product_name'], OBJECT, 'product' ) ): ?>
	<script>
		var product_name = '<?php echo $_GET['product_name']; ?>';
		var product_category = '<?php echo $_GET['product_category']; ?>';
	</script>
<?php endif; ?>

<?php global $wp; ?>

<div class="row">
	<div class="col-md-5">
		<div class="mb-4 h4 text-uppercase">Product Category</div><select name="product-category-selector" id="product-category-selector"></select>
	</div>
	<div class="col-md-5">
		<div class="mb-4 h4 text-uppercase">Product</div><select name="product-selector" id="product-selector"></select>
	</div>
	<div class="col-md-2 d-flex align-items-end">
		<a id="go-faq" href="" class="btn btn-primary">GO</a>
	</div>
</div>

<script>

	$(document).ready(function(){

    		var data = {
				'action': 'lg_get_product_categories',
			};

			jQuery.get('<?php echo admin_url('admin-ajax.php'); ?>', data, function(response) {
				var product_category_element = $('#product-category-selector');
				var product_element = $('#product-selector');

				product_category_element.children().remove();
				product_category_element.append('<option value=""></option>');

				for(var i=0; i<response.length; i++){
					product_category_element.append("<option value='" + response[i].slug + "'>" + response[i].name + "</option>");
				}

				if(product_category){
					product_category_element.find('option[value="' + product_category + '"]').prop('selected', true);

					//get product by category
					data = {
						'action': 'lg_get_products_by_categories',
						'data': {'product_cat': product_category}
					};

					jQuery.get('<?php echo admin_url('admin-ajax.php'); ?>', data, function(response) {
						product_element.children().remove();
						for(var i=0; i<response.length; i++){
							if(product_name == response[i].post_name){
								product_element.append("<option value='/faq/?product_name=" + response[i].post_name + "&&product_category=" + product_category + "' selected>" + response[i].post_title + "</option>");
							}else{
								product_element.append("<option value='/<?php echo $wp->request; ?>/?product_name=" + response[i].post_name + "&&product_category=" + product_category + "'>" + response[i].post_title + "</option>");
							}
						}
					});
				}else{
					product_element.children().remove();

					data = {
						'action': 'lg_get_product_name_by_slug',
						'data': {'product_slug': product_name}
					};

					jQuery.get('<?php echo admin_url('admin-ajax.php'); ?>', data, function(response) {
						product_element.append("<option>" + response + "</option>");
					});
				}
			});

			$('#go-faq').on('click', function(e){
				e.preventDefault();
				window.location.replace($('#product-selector').val());
			});

			$('body').on('change', '#product-category-selector', function(){
				var current_product_category = $(this).val();
				var product_element = $('#product-selector');

				if(current_product_category && current_product_category!= ''){
					data = {
						'action': 'lg_get_products_by_categories',
						'data': {'product_cat': current_product_category}
					};

					jQuery.get('<?php echo admin_url('admin-ajax.php'); ?>', data, function(response) {
						console.log(response);
						product_element.children().remove();
						for(var i=0; i<response.length; i++){
							if(product_name == response[i].post_name){
								product_element.append("<option value='/faq/?product_name=" + response[i].post_name + "&&product_category=" + current_product_category + "' selected>" + response[i].post_title + "</option>");
							}else{
								product_element.append("<option value='/<?php echo $wp->request; ?>/?product_name=" + response[i].post_name + "&&product_category=" + current_product_category + "'>" + response[i].post_title + "</option>");
							}
						}
					});
				}
			});

    });

</script>