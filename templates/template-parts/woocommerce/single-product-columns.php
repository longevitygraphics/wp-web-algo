<?php

$layouts       = $description_field['layouts'];
$content       = $description_field['content'];
$content_left  = $description_field['content_left'];
$content_right = $description_field['content_right'];
$column_width  = $description_field['column_width'];
$column_left   = $column_width['column_left'][0];
$column_right  = $column_width['column_right'][0];

?>

<div class="py-5">
	<?php if ( $layouts == 'full-width' ): ?>
		<?php echo $content; ?>
	<?php else: ?>
		<div class="row">
			<div class="col-12 col-lg-<?php echo $column_left; ?>">
				<?php echo $content_left; ?>
			</div>
			<div class="col-12 mt-4 mt-lg-0 col-lg-<?php echo $column_right; ?>">
				<?php echo $content_right; ?>
			</div>
		</div>
	<?php endif; ?>
</div>
