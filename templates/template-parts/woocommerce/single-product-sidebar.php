<?php
$terms = get_the_terms( $post, 'product_cat' );

$current_page_url     = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$add_to_cart_page_url = strtok( $current_page_url, "?" );
$add_to_cart_page_url .= '/buy/';
?>

<div class="product-sidebar">
	<div>
		<div>
			<div class="font-weight-bold h5 text-dark">Product Resources</div>

			<div>
				<?php $download_link = get_field( 'download_link' ); ?>
				<?php $faq_title = get_field('faq_title')?>
				<?php if ( $download_link && is_array( $download_link ) && array_key_exists( 'url', $download_link ) ): ?>
					<?php
					$download_link_url    = $download_link['url'];
					$download_link_title  = $download_link['title'];
					$download_link_target = $download_link['target'] ? $download_link['target'] : '_self';
					?>
				<?php endif; ?>
				<?php if ( have_rows( 'sidebar_links' ) ): ?>
					<?php while ( have_rows( 'sidebar_links' ) ): the_row(); ?>
						<?php
							$link_type = get_sub_field( "link_type" );
						?>
						<?php if ( $link_type == 'Page or External Link' ): ?>
							<?php
							$link_text = get_sub_field('link_text');
							$page = get_sub_field( 'page' );
							if ( $page && is_array( $page ) && array_key_exists( 'url', $page ) ) {
								$page_url    = $page['url'];
								$page_title  = $page['title'];
								$page_target = $page['target'] ? $page['target'] : '_self';
							}
							?>
							<a href="<?php echo esc_url( $page_url ); ?>"
							   target="<?php echo esc_attr( $page_target ); ?>">
							    <?php if ($link_text): ?>
							    	<?php echo $link_text; ?>
							    <?php else: ?>
							   		<?php echo esc_html( $page_title ); ?>
							    <?php endif ?>
							</a>
						<?php endif ?>
						<?php if ( $link_type == 'Form' ): ?>

							<?php
								$file = get_sub_field( 'form' );
								$link_text = get_sub_field( "link_text" );
							?>
							<?php if ( $file && is_array( $file ) && array_key_exists( 'url', $file ) ): ?>
								<a href="/download/?media=<?php echo $file['id']; ?>"
								   target="<?php echo esc_attr( $download_link_target ); ?>">
								   <?php if ($link_text): ?>
								   		<?php echo $link_text; ?>
								   <?php else: ?>
								   		<?php echo $file['title']; ?>
								   <?php endif ?>
								</a>
							<?php endif ?>
						<?php endif ?>
						<?php if ( $link_type == 'File' ): ?>

							<?php
								$file = get_sub_field( 'file' );
								$link_text = get_sub_field( "link_text" );
							?>
							<?php if ( $file && is_array( $file ) && array_key_exists( 'url', $file ) ): ?>
								<a href="<?php echo $file['url']; ?>" target="_blank"><?php if ($link_text): ?>
								   	<?php echo $link_text; ?>
								<?php else: ?>
								   	<?php echo $file['title']; ?>
								<?php endif ?>
								</a>
							<?php endif ?>
						<?php endif ?>
					<?php endwhile; ?>
				<?php endif; ?>
				<?php if ($faq_title): ?>
					<a href="/faq/?product_name=<?php echo $current_post->post_name; ?><?php if ( $terms && is_array( $terms ) ): ?>&&product_category=<?php echo $terms[0]->slug; ?><?php endif; ?>">
							<?php echo $faq_title; ?>
					</a>
				<?php endif; // end of if ($faq_title):  ?>
			</div>
				
			<?php
			$current_product_id = get_the_ID();
			$product            = wc_get_product( $current_product_id );
			// print_r($product);
			$product_price = $product->get_price();
			$formatted_price = $product->get_price_html();
			?>

			<div>
				<?php if($product->get_price() && $product->is_purchasable()) : ?>
				<a href="//<?php echo $add_to_cart_page_url; ?>"
				   class="btn btn-primary buy-online">BUY ONLINE</a>
				<?php endif; ?>
				<a href="mailto:<?php echo do_shortcode( '[lg-email]' ); ?>"><i class="fas fa-envelope"></i> Email
					Us</a>
				<a href="tel:<?php echo do_shortcode( '[lg-phone-main]' ); ?>"><i class="fas fa-phone"></i> Call Us</a>
			</div>
			
			<div class="sidebar-price">Product
				MSRP: <?php echo $formatted_price; ?></div>
			<?php do_action( 'lg_woocommerce_product_sidebar' ); ?>
		</div>
		<div>
			<i class="fas fa-times"></i>
		</div>
	</div>
</div>
