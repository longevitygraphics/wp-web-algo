<?php get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main class="container py-5">
				<?php while (have_posts()) : the_post(); ?>
					<?php // the_content(); ?>
					<ul class="shop-featured-products d-flex flex-wrap">
					<?php
						$args = array(
							'post_type' => 'product',
							'posts_per_page' => 12,
							'tax_query' => array(
									array(
										'taxonomy' => 'product_visibility',
										'field'    => 'name',
										'terms'    => 'featured',
									),
								),
							);
						$loop = new WP_Query( $args );
						?>
						<?php if( $loop->have_posts() ) : ?>
							<?php while( $loop->have_posts() ) :  $loop->the_post(); ?>
								<div class='featured-product col-md-6 col-lg-4 col-xl-3'>
									<?php
										$thumb_id = get_post_thumbnail_id(get_the_ID());
										$image_url = get_the_post_thumbnail_url();
										$alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
										$price = get_post_meta( get_the_ID(), '_price', true );
									?>
									<div class='featured-product-image'>
										<img src="<?php echo $image_url; ?>" alt="<?php echo $alt;?>">
									</div>
									<h3 class='featured-product-link'><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<?php
										if(has_excerpt()){
											the_excerpt();
										}
									?>
									<?php
										if($price){
											echo wc_price( $price );
										}
									?>
									<div class="featured-add-to-cart">
										<?php woocommerce_template_loop_add_to_cart(); ?>
									</div>
								</div>
								
							<?php endwhile;?>
						<?php endif;?>
						<?php wp_reset_postdata(); ?>
					</ul><!--/.products-->
					
				<?php endwhile; ?>
			</main>
		</div>
	</div>

<?php get_footer(); ?>