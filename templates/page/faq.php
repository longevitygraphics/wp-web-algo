<?php

get_header(); ?>

	<main>

		<div class="container pt-5 faq-filter"><?php get_template_part("/templates/template-parts/page/product-category-filter"); ?></div>

		<?php if (isset($_GET['product_name']) && $post = get_page_by_path( $_GET['product_name'], OBJECT, 'product' ) ): ?>

			<div class="py-5">
				<div class="container">
					<?php 

						$faqs = get_field('faq', $post->ID); 
						$faq_group_index = 0;

						foreach ($faqs as $key => $faq) {
							switch ($faq['acf_fc_layout']) {
								case 'group_title':
									$faq_group_title = $faq['group_title'];
									include(locate_template('/layouts/layouts/faq/faq-title.php')); 
									break;
								
								case 'group_faq':
									$faqs = $faq['faqs'];
									include(locate_template('/layouts/layouts/faq/faq-group.php')); 
									$faq_group_index++;
									break;
							}
						}

					?>
				</div>
			</div>

		<?php else: ?>
			<div class="py-5 min-screen-height">
				<div class="container">
					<h1>Page not found</h1>
					<h2>please make sure you entered the correct url.</h2>
				</div>
			</div>
		<?php endif; ?>

	</main>

	<script>

		$(document).ready(function(){

	    		var data = {
					'action': 'lg_get_product_categories',
				};

				jQuery.get('<?php echo admin_url('admin-ajax.php'); ?>', data, function(response) {
					//location.reload();
					console.log(response);
				});

	    });

	</script>

<?php get_footer(); ?>