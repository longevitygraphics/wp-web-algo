<?php

//check if the page is "add to cart"
$subpage_var         = get_query_var( 'subpage' );
$page_is_add_to_cart = ( isset( $subpage_var ) and ! empty( $subpage_var ) ) ? true : false;


if($page_is_add_to_cart){
	add_filter( 'body_class','lg_body_classes' );
	function lg_body_classes( $classes ) {
		$classes[] = 'page-is-add-to-cart';
		return $classes;
	}
}







?>

<?php get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>
				<?php if ( have_posts() ) : ?>
					<?php woocommerce_content(); ?>
				<?php endif ?>
			</main>
		</div>
	</div>

<?php get_footer(); ?>
