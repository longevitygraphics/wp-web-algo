<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
	<?php do_action('wp_content_bottom'); ?>
	</div>
	
	<?php do_action('wp_body_end'); ?>
	<?php $lg_option_footer_site_legal = get_option('lg_option_footer_site_legal'); ?>

	<footer id="site-footer">
		<div class="container">
			<div class="row">
				<div class="footer-logo col-md-6 col-lg-3 mb-4">
					<a href="/"><img src="<?php echo do_shortcode('[lg-logo-alt]');  ?>"></a>
				</div>
				<div class="footer-contact col-md-6 col-lg-3 mb-4">
					<h3>Contact</h3>
					<div>
						<p><a href="mailto:<?php echo do_shortcode('[lg-email]'); ?>">Email Us</a></p>
						<p><a href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo do_shortcode('[lg-phone-main]'); ?></a></p>
					</div>
				</div>
				<div class="footer-navigation col-md-6 col-lg-3 mb-4">
					<h3>Navigation</h3>
					<?php 
			            $footerNav = array(
			              'menu'              => 'footer-nav',
			              'depth'             => 1,
			              'container'         => 'div'
			            );
			            wp_nav_menu($footerNav);
			        ?>
				</div>
				<div class="footer-resource col-md-6 col-lg-3">
					<h3>Resource</h3>
					<?php 
			            $footerResource = array(
			              'menu'              => 'footer-resource',
			              'depth'             => 1,
			              'container'         => 'div'
			            );
			            wp_nav_menu($footerResource);
			        ?>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>

<?php do_action('document_end'); ?>
